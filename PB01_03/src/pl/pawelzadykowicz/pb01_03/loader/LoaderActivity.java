package pl.pawelzadykowicz.pb01_03.loader;

import java.util.List;

import pl.pawelzadykowicz.pb01_03.DataItem;
import pl.pawelzadykowicz.pb01_03.MyContentAdapter;
import pl.pawelzadykowicz.pb01_03.R;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.view.Menu;
import android.widget.ListAdapter;
import android.widget.ListView;

public class LoaderActivity extends FragmentActivity implements LoaderCallbacks<List<DataItem>> {

	/**
	 * id loadera.
	 */
	private static final int MY_LOADER = 1;
	private static final String FILENAME_KEY = "filename_key";
	private ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loader);
		listView = (ListView) findViewById(R.id.listView1);
		LoaderManager lm = getSupportLoaderManager();
		Bundle args = new Bundle();
		args.putString(FILENAME_KEY, "dictionary.txt");
		lm.initLoader(MY_LOADER, args , this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.loader, menu);
		return true;
	}

	@Override
	public android.support.v4.content.Loader<List<DataItem>> onCreateLoader(
			int arg0, Bundle args) {
		String filename = args.getString(FILENAME_KEY);
		return new MyContentLoader(getApplicationContext(), filename);
	}

	@Override
	public void onLoadFinished(
			android.support.v4.content.Loader<List<DataItem>> loader,
			List<DataItem> data) {
		ListAdapter adapter = new MyContentAdapter(getApplicationContext(), data);
		listView.setAdapter(adapter);
	}

	@Override
	public void onLoaderReset(
			android.support.v4.content.Loader<List<DataItem>> loader) {
		MyContentAdapter adapter = (MyContentAdapter) listView.getAdapter();
		if (adapter != null)
		{
			adapter.notifyDataSetInvalidated();
		}
	}
	
}