package pl.pawelzadykowicz.pb01_03;


public class DataItem {
	/**
	 * typ wiersza.
	 * 
	 * @author zadykowiczp
	 * 
	 */
	public enum RowType {
		/**
		 * nagłówek.
		 */
		HEADER ,
		/**
		 * słowo ze słownika.
		 */
		WORD
	}

	public DataItem(RowType type, long id) {
		this.id = id;
		this.type = type;
	}

	/**
	 * id elementu danych.
	 */
	private long id;

	public long getId() {
		return id;
	}

	/**
	 * typ wiersza dla elementu danych.
	 */
	protected RowType type;

	public RowType getType() {
		return type;
	}
}
