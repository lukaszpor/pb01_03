package pl.pawelzadykowicz.pb01_03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import android.nfc.Tag;
import android.os.AsyncTask;
import android.util.Log;



public class LoadDataTask extends AsyncTask<Void, Void, List<DataItem>> 
{
	public interface LoadCallback{

		InputStream getInputStream(String filename) throws IOException;

		void onDataLoaded(List<DataItem> result);
		
	}
	private String filename;
	
	private LoadCallback callback;
	
	private static final String TAG = LoadDataTask.class.getName();
	
	
	public LoadDataTask(String filename, LoadCallback callback) {
		// TODO Auto-generated constructor stub
		this.filename = filename;
		this.callback = callback;
	}
	
	
	@Override
	protected List<DataItem> doInBackground(Void... params) {
		
		List<DataItem> rat = new LinkedList<DataItem>();
		InputStream is = null;
		try
		{
			is = callback.getInputStream(filename);
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			long id = 0;
			while((line = reader.readLine())!= null){
				++id;
				DataItem dataItem = new WordItem(id, line, line.length());
				rat.add(dataItem);
			}
			
			return rat;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "b�ad czytania pliku: " + e.getMessage());
			return null;
		}
		finally
		{
			if(is != null)
			{
				try
				{
					is.close();
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	protected void onPostExecute(List<DataItem> result){
		callback.onDataLoaded(result);
	}
}
