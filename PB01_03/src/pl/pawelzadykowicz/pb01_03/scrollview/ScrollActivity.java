package pl.pawelzadykowicz.pb01_03.scrollview;

import pl.pawelzadykowicz.pb01_03.R;
import pl.pawelzadykowicz.pb01_03.WordItem;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScrollActivity extends Activity /*implements LoadCallback */{

	private String filename = "dictionary_small.txt";
	private LinearLayout llList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scroll);
		llList = (LinearLayout) findViewById(R.id.llList);
//		LoadDataTask task = new LoadDataTask(filename , this);
//		task.execute();
	}

	




	/**
	 * tworzy wiersz ze słowem ze słownika.
	 * @param item element danych.
	 */
	private void pupulateWordRow(WordItem item) {
		View row = View.inflate(this, R.layout.row, null);
		TextView tvId = (TextView) row.findViewById(R.id.tvId);
		tvId.setText(String.valueOf(item.getId()));
		
		TextView tvLength = (TextView) row.findViewById(R.id.tvLength);
		tvLength.setText(String.valueOf(item.getLength()));
		
		TextView tvWord= (TextView) row.findViewById(R.id.tvWord);
		tvWord.setText(String.valueOf(item.getWord()));
		llList.addView(row);
	}

//	@Override
//	public InputStream getInputStream(String filename) throws IOException {
//		return getAssets().open(filename);
//	}

}
