package pl.pawelzadykowicz.pb01_03;

import pl.pawelzadykowicz.pb01_03.DataItem.RowType;

public class WordItem extends DataItem {

	
	private String word;
	private int length;

	public WordItem(long id, String word, int length) {
		super(RowType.WORD, id);
		this.word = word;
		this.length = length;
	}

	public String getWord() {
		return word;
	}

	public int getLength() {
		return length;
	}
	

}
