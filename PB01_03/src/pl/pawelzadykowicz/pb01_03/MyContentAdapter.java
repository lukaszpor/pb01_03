package pl.pawelzadykowicz.pb01_03;

import java.util.List;

import android.content.Context;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyContentAdapter extends BaseAdapter {

	
	private static class ViewHolder{
		TextView tvId;
		TextView tvWord;
		TextView tvLength;
	}
	
	private Context ctx;
	private List<DataItem> data;
	
	public MyContentAdapter(Context ctx, List<DataItem> result) {
		this.ctx = ctx;
		this.data = result;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public WordItem getItem(int position) {
		return (WordItem)data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = null;
		if(convertView != null)
		{
			view = convertView;
		}
		else
		{
			view = View.inflate(ctx, R.layout.row, null);
			ViewHolder vh = new ViewHolder();
			vh.tvId = (TextView) view.findViewById(R.id.tvId);
			vh.tvLength = (TextView)view.findViewById(R.id.tvLength);
			vh.tvWord = (TextView)view.findViewById(R.id.tvWord);
			view.setTag(vh);
		}
		ViewHolder vh = (ViewHolder) view.getTag();
		WordItem item = getItem(position);
		vh.tvId.setText(String.valueOf(item.getId()));
		vh.tvLength.setText(String.valueOf(item.getLength()));
		vh.tvWord.setText(item.getWord());
		return view;
	}
}
