package pl.pawelzadykowicz.pb01_03;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import pl.pawelzadykowicz.pb01_03.DataItem.RowType;
import pl.pawelzadykowicz.pb01_03.LoadDataTask.LoadCallback;
import pl.pawelzadykowicz.pb01_03.loader.LoaderActivity;
import pl.pawelzadykowicz.pb01_03.scrollview.ScrollActivity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity03 extends FragmentActivity implements 
		OnItemClickListener, LoadCallback {

	private ListView lvContent;
	private String dataFilename = "dictionary.txt";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lvContent = (ListView) findViewById(R.id.listView1);
		LoadDataTask loadDataTask = new LoadDataTask(dataFilename, this);
		loadDataTask.execute();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
		case R.id.action_scrollview: {
			Intent intent = new Intent(this, ScrollActivity.class);
			startActivity(intent);
			return true;
		}
		case R.id.action_loader: {
			Intent intent = new Intent(this, LoaderActivity.class);
			startActivity(intent);
			return true;
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		DataItem clickedItem = (DataItem) parent.getItemAtPosition(position);
		if (clickedItem.getType().equals(RowType.WORD)) {
			Toast.makeText(
					this,
					getString(R.string.clicked_item_notification,
							((WordItem) clickedItem).getWord()),
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public InputStream getInputStream(String filename) throws IOException {
		// TODO Auto-generated method stub
		AssetManager assets = getAssets();
		
		return assets.open(filename);
	}

	@Override
	public void onDataLoaded(List<DataItem> result) {
		// TODO Auto-generated method stub
		MyContentAdapter adapter = new MyContentAdapter(this, result);
		lvContent.setAdapter(adapter);
		lvContent.setOnItemClickListener(this);
	}
	

}
